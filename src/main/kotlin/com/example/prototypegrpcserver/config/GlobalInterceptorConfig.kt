package com.example.prototypegrpcserver.config

import com.example.prototypegrpcserver.interceptor.RequestValidationInterceptor
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
class GlobalInterceptorConfig {
    @GrpcGlobalServerInterceptor
    fun requestValidationInterceptor(): RequestValidationInterceptor = RequestValidationInterceptor()
}
