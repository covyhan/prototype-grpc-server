package com.example.prototypegrpcserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PrototypeGrpcServerApplication

fun main(args: Array<String>) {
    runApplication<PrototypeGrpcServerApplication>(*args)
}
