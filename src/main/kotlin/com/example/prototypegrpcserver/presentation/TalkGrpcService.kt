package com.example.prototypegrpcserver.presentation

import com.example.prototypegrpcserver.interceptor.RequestValidationInterceptor
import com.example.talk.proto.TalkGrpcKt
import com.example.talk.proto.TalkOuterClass
import com.example.talk.proto.sendTalkResponse
import net.devh.boot.grpc.server.service.GrpcService
import org.slf4j.LoggerFactory
import java.util.*

@GrpcService
class TalkGrpcService : TalkGrpcKt.TalkCoroutineImplBase() {
    val random = Random()

    override suspend fun sendTalk(request: TalkOuterClass.SendTalkRequest): TalkOuterClass.SendTalkResponse =
        sendTalkResponse {
            result = "OK"
            messageId = "message_id_${random.nextLong()}"
        }
}
