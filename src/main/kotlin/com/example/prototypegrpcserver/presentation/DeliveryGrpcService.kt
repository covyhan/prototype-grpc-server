package com.example.prototypegrpcserver.presentation

import com.example.delivery.proto.DeliveryGrpcKt
import com.example.delivery.proto.DeliveryOuterClass
import com.example.delivery.proto.getPurchaseInfoResponse
import net.devh.boot.grpc.server.service.GrpcService
import java.util.*

@GrpcService
class DeliveryGrpcService : DeliveryGrpcKt.DeliveryCoroutineImplBase() {
    val random = Random()

    override suspend fun getPurchaseInfo(request: DeliveryOuterClass.GetPurchaseInfoRequest): DeliveryOuterClass.GetPurchaseInfoResponse = getPurchaseInfoResponse {
        purchaseId = request.purchaseId
        cvsType = 1
        deliveryStatus = DeliveryOuterClass.GetPurchaseInfoResponse.DeliveryStatus.DONE
        orderId = random.nextLong()
        deliveryTrackNum = "delivery_track_num_${random.nextLong()}"
    }
}