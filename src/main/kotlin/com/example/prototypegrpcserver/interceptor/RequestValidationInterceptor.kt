package com.example.prototypegrpcserver.interceptor

import io.envoyproxy.pgv.ReflectiveValidatorIndex
import io.envoyproxy.pgv.ValidationException
import io.grpc.*
import io.grpc.ForwardingServerCallListener.SimpleForwardingServerCallListener
import org.slf4j.LoggerFactory

class RequestValidationInterceptor : ServerInterceptor {
    override fun <ReqT : Any?, RespT : Any?> interceptCall(
        call: ServerCall<ReqT, RespT>?,
        headers: Metadata?,
        next: ServerCallHandler<ReqT, RespT>?
    ): ServerCall.Listener<ReqT> = object: SimpleForwardingServerCallListener<ReqT>(next?.startCall(call, headers)!!) {
        val validatorIndex = ReflectiveValidatorIndex()
        var validationException: ValidationException? = null

        override fun onMessage(message: ReqT) {
            try {
                validatorIndex.validatorFor<ReqT>(message).assertValid(message)
                super.onMessage(message)
            } catch (e: ValidationException) {
                log.info("onMessage. validation exception. message - ${e.message}, reason - ${e.reason}")
                validationException = e
            }
        }

        override fun onHalfClose() {
            if (validationException == null) {
                super.onHalfClose()
                return
            }

            call?.close(Status.INVALID_ARGUMENT.withDescription(validationException!!.reason), Metadata())
        }
    }

    companion object {
        val log = LoggerFactory.getLogger(RequestValidationInterceptor::class.java);
    }
}
